const User = require("../models/user");

exports.createUser = async (req, res) => {
  
  User.findOne({ email: req.body.email }).exec((err, user) => {
    if (user) {
      return res.status(400).json({
        error: "Email is taken",
      });
    }
  });
  const user = new User({
    name: req.body.name,
    email: req.body.email,
  });
  try {
    await user.save();
    res.status(201).json(user);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
