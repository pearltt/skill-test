const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
require("dotenv").config();

// route
const blogRoutes = require("./routes/blog");
const userRoutes = require("./routes/user");

// app
const app = express();

// database
mongoose
  .connect(process.env.DATABASE, { useNewUrlParser: true })
  .then(() => console.log("Database connection success!"))
  .catch((err) => {
    console.log(err);
  });

// middleware

app.use(express.json());

// cors
app.use(cors());

// routes
app.use("/api", blogRoutes);
app.use("/api", userRoutes);

// port
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
